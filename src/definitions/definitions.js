let section = [
  {
    label: "name",
    value: "name"
  },
  {
    label: "portrait",
    value: "portrait"
  },
  {
    label: "description",
    value: "description"
  },
  {
    label: "table",
    value: "table"
  },
  {
    label: "stats",
    value: "stats"
  },
  {
    label: "list",
    value: "list"
  }
];

let schema = [
  {
    type: "name",
    accept: ["text", "icon"],
    default: ["icon", "text"]
  },
  {
    type: "portrait",
    accept: ["image"],
    default: ["image"]
  },
  {
    type: "description",
    accept: ["icon", "text"],
    default: ["text"]
  },
  {
    type: "stats",
    accept: ["text"],
    default: ["text"]
  },
  {
    type: "table",
    accept: ["text"],
    default: ["text"]
  },
  {
    type: "list",
    accept: ["list"],
    default: ["list"]
  }
];

let element = [
  {
    type: "text",
    label: "",
    showLabel: false,
    value: "",
    order: ""
  },
  {
    type: "icon",
    label: "",
    showLabel: false,
    value: "",
    order: ""
  },
  {
    type: "image",
    label: "",
    showLabel: false,
    value: "",
    order: ""
  },
  {
    type: "label",
    label: "",
    showLabel: false,
    value: "",
    order: ""
  },
  {
    type: "list",
    schema: [
      {
        type: "text",
        label: "name",
        value: ""
      },
      {
        type: "image",
        label: "image",
        value: ""
      },
      {
        type: "text",
        label: "info",
        value: ""
      }
    ],
    label: "",
    showLabel: false,
    value: [],
    order: ""
  }
];

let list = [
  {
    label: "text",
    value: "text"
  },
  {
    label: "image",
    value: "image"
  },
  {
    label: "label",
    value: "label"
  }
];

export default {
  section,
  schema,
  element,
  list
};
