// let view = {
//   page: "",
//   children: []
// };

let category = {
  name: "",
  description: "",
  thumbnail: "",
  layout: ""
};

let viewChildren = {
  page: "",
  type: "",
  key: "",
  data: [
    {
      type: "",
      label: "",
      value: "",
      key: ""
    }
  ],
  children: []
};

let sectionOne = {
  parent: "Dragon",
  type: "name",
  order: "0",
  content: [
    {
      type: "icon",
      label: "Flame",
      value: "Flame",
      order: "0"
    },
    {
      type: "text",
      label: "name",
      value: "Agni",
      order: "0"
    },
    {
      type: "text",
      label: "name",
      value: "Antonio",
      order: "0"
    }
  ],
  children: []
};

let sectionTwo = {
  parent: "Dragon",
  type: "portrait",
  order: "0",
  content: [
    {
      type: "image",
      label: "portrait",
      value:
        "https://d1u5p3l4wpay3k.cloudfront.net/dragalialost_gamepedia_en/thumb/4/49/210016_01_portrait.png/400px-210016_01_portrait.png?version=726ace0461f4fcf8dfc32293bc4cc9ea",
      order: "0"
    }
  ],
  children: []
};

let sectionThree = {
  parent: "Dragon",
  type: "description",
  order: "0",
  schema: [],
  content: [
    {
      type: "text",
      label: "description",
      value:
        'Known as the Firelord, he describes fire as "an all-consuming greed" and demands offerings from humans to sate his appetite. Dignified on the surface, he\'s selfish at heart—though he will always protect those who make offerings to him.',
      order: "0"
    }
  ],
  children: []
};

export default {
  page: "Dragon",
  children: [sectionOne, sectionTwo, sectionThree]
};
