const routes = [
  {
    path: "/",
    component: () => import("layouts/MyLayout.vue"),
    children: [{ path: "", component: () => import("pages/app/Index.vue") }]
  },
  {
    path: "/admin/brawlstars",
    component: () => import("layouts/MyLayout.vue"),
    children: [{ path: "", component: () => import("pages/games/brawlstars/admin/brawlers.vue") }]
  },
  {
    path: "/admin",
    component: () => import("layouts/AdminLayout.vue"),
    children: [
      { path: "", component: () => import("pages/admin/Game.vue") },
      {
        path: "game/:game",
        name: "Category",
        component: () => import("pages/admin/Category.vue")
      },
      {
        path: "game/:game/view/:id",
        name: "View",
        component: () => import("pages/admin/View.vue")
      },
      {
        path: "game/:game/model/:id",
        name: "Model",
        component: () => import("pages/admin/Model.vue")
      },
      {
        path: "game/:game/data/:id",
        name: "Data",
        component: () => import("pages/admin/Data.vue")
      },
      {
        path: "game/:game/model/:id/edit/:edit",
        name: "Model",
        component: () => import("pages/admin/Model.vue")
      }
    ]
  },
  {
    path: "/",
    component: () => import("layouts/MyLayout.vue"),
    children: [
      {
        path: ":game",
        name: "Game",
        component: () => import("pages/app/Game.vue")
      },
      {
        path: ":game/:category",
        name: "Category",
        component: () => import("pages/app/Category.vue")
      }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
