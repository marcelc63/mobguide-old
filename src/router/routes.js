const routes = [
  {
    path: "/",
    component: () => import("layouts/MyLayout.vue"),
    children: [
      {
        path: "",
        component: () => import("pages/games/brawlstars/guide/Index.vue")
      },
      {
        path: "events",
        component: () => import("pages/games/brawlstars/guide/Events.vue")
      },
      {
        path: "brawler/:slug",
        component: () => import("pages/games/brawlstars/guide/Brawler.vue")
      },
      {
        path: "event/:slug",
        component: () => import("pages/games/brawlstars/guide/Event.vue")
      }
    ]
  },
  {
    path: "/admin/brawlstars",
    component: () => import("layouts/MyLayout.vue"),
    children: [
      {
        path: "",
        component: () => import("pages/games/brawlstars/admin/Brawlers.vue")
      },
      {
        path: "events",
        component: () => import("pages/games/brawlstars/admin/Events.vue")
      }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
